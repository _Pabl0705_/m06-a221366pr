# EXECUCIÓ

$ docker run --rm --name pam.edt.org -h pam.edt.org --privileged -d a221366pr/pam23:ldap

En aquesta pràctica s'ha utilitzat la base de dades ldap per realitzar l'autorització
amb el pam. Per engegar la base de dades LDAP:

$ docker run --rm --name ldap.edt.org -h ldap.edt.org -d a221366pr/ldap23:latest

S'ha modificat el fitxer client ldap: /etc/ldap/ldap.conf, fent que apunti
directament a la base de dades ldap amb hostname ldap.edt.org

Quan s'inicia sessio dins un usuari (local o del LDAP) es crea un directori
home propi, amb un volum tmpfs de 100M


