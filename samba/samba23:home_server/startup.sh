#!/bin/bash

for user in unix01 unix02 unix03
do
	useradd -m -s /bin/bash $user
	echo -e "$user\n$user" | passwd $user
done

cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf

bash /opt/docker/ldapusers.sh

/usr/sbin/nscd
/usr/sbin/nslcd

# Share public
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir -p /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# copiar la configuració
cp /opt/docker/smb.conf /etc/samba/smb.conf

for user in samba01 samba02 samba03 samba04 samba05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done

chmod +x /opt/docker/fitxer.sh
bash /opt/docker/fitxer.sh

/usr/sbin/smbd 
/usr/sbin/nmbd -F

sleep infinity
