# EXECUCIÓ

$ docker run --rm --name ldap -p 389:389 -h ldap.edt.org -d a221366pr/ldap23:grups

### Directori del container ldap23:grups.

##### Modificacions:

S'ha afegit un nou ou anomenat grups, el qual englova noves entitats creades que
són grups dels cicles d'informàtica. S'ha assignat als diferents usuaris de la
base de dades aquests grups. 
