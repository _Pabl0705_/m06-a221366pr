#!/bin/bash

# echo de prova, provem si s'executa el startup al inicialitzar el container

echo "Benvingut!"

# Eliminen els contingut dels directoris de configuració i de dades

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*

# Generar el directori de configuració dinàmica slapd.d a partir del fitxer
# de configuració slapd.conf (dins el directori de context)

slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

# Injectem la base de dades a baix nivell de l'organització dc=edt,dc=org

slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif

# Canviem el propietari i grup propietari del directori de dades a l'usuari
# "openldap" i grup "openldap"

chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

# Engeguem el servei en detach (debug) per mantenir-lo en foreground

/usr/sbin/slapd -d0



